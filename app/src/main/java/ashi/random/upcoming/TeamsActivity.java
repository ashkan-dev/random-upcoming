package ashi.random.upcoming;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ListAdapter;
import android.widget.TextView;

import java.io.File;
import java.io.FileOutputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class TeamsActivity extends BaseAdapter implements ListAdapter {
    private ArrayList<String> list = new ArrayList<String>();
    private Context context;
    private Button btnSelectTeam;
    private TextView selectedText;

    public TeamsActivity(ArrayList<String> list, Context context, Button select, TextView selected) {
        this.list = list;
        this.context = context;
        this.btnSelectTeam = select;
        this.selectedText = selected;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int pos) {
        return list.get(pos);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        View view = convertView;
        if (view == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = inflater.inflate(R.layout.teams_list, null);
        }

        TextView itemText = view.findViewById(R.id.item);
        itemText.setText(list.get(position));

        Button deleteBtn = view.findViewById(R.id.delete_btn);

        deleteBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                list.remove(position);
                notifyDataSetChanged();
                save();

                if (list.size() == 0) {
                    btnSelectTeam.setEnabled(false);
                    selectedText.setText("");
                }
            }
        });

        return view;
    }

    private void save() {
        try {
            File file = new File(context.getFilesDir(), context.getPackageName());
            File team_file = new File(file, context.getResources().getString(R.string.teams));
            FileOutputStream f = new FileOutputStream(team_file);
            ObjectOutputStream s = new ObjectOutputStream(f);
            final Map<String, String> teamMap = new HashMap<>();

            for (String object: list) {
                teamMap.put(object, object);
            }

            s.writeObject(teamMap);
            s.close();
            f.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}