package ashi.random.upcoming;

import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.icu.text.RelativeDateTimeFormatter;
import android.net.Uri;
import android.os.Bundle;
import android.os.FileUtils;
import android.text.Layout;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

public class MainActivity extends AppCompatActivity {
    final Context context = this;
    final ArrayList<String> list = new ArrayList<>();

    TeamsActivity teamsActivity;

    private static final int FILE_SELECT_CODE = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        toolbar.setTitle("");
        setSupportActionBar(toolbar);

        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog.Builder builder = new AlertDialog.Builder(context);
                builder.setTitle(R.string.enter_team);
                LayoutInflater inflater = getLayoutInflater();
                final View team_select = inflater.inflate(R.layout.add_team, null);
                final EditText team = team_select.findViewById(R.id.team);
                final ArrayList<String> list_temp = list;

                builder.setView(team_select)
                        .setPositiveButton(R.string.apply, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int id) {
                                String teamName = team.getText().toString();
                                Map<String, String> teamMap = new HashMap<>();
                                File file = new File(context.getFilesDir(), context.getPackageName());

                                if (!file.exists()) {
                                    file.mkdir();
                                } else {
                                    File team_file = new File(file, getString(R.string.teams));
                                    teamMap.put(teamName, teamName);

                                    try {
                                        FileInputStream f = new FileInputStream(team_file);
                                        ObjectInputStream s = new ObjectInputStream(f);
                                        HashMap<String, String> team_read_map = (HashMap<String, String>) s.readObject();

                                        if (team_read_map.containsKey(teamName)) {
                                            Toast.makeText(context, "Team already exists in list", Toast.LENGTH_LONG).show();
                                            return;
                                        } else {
                                            teamMap.putAll(team_read_map);
                                        }

                                        s.close();
                                        f.close();
                                    } catch (IOException | ClassNotFoundException e) {
                                        Snackbar.make(team_select, getString(R.string.err_msg), Snackbar.LENGTH_LONG)
                                                .setAction("Action", null).show();
                                        return;
                                    }
                                }

                                try {
                                    File team_file = new File(file, getString(R.string.teams));
                                    FileOutputStream f = new FileOutputStream(team_file);
                                    ObjectOutputStream s = new ObjectOutputStream(f);
                                    s.writeObject(teamMap);
                                    s.close();
                                    f.close();
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }

                                list_temp.add(teamName);
                                setList(list_temp);
                            }
                        });

                AlertDialog dialog = builder.create();
                dialog.show();
            }
        });

        Button button = findViewById(R.id.gen_unique);
        button.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Button copy = findViewById(R.id.copy);
                copy.setVisibility(View.GONE);

                EditText start = findViewById(R.id.start_num);
                EditText end = findViewById(R.id.end_num);
                String strStart = start.getText().toString();
                String strEnd = end.getText().toString();

                if (!strStart.isEmpty() && !strEnd.isEmpty()) {
                    int numStart = Integer.parseInt(strStart);
                    int numEnd = Integer.parseInt(strEnd);

                    if (numStart >= numEnd) {
                        Snackbar.make(v, "Please select numbers carefully", Snackbar.LENGTH_LONG)
                                .setAction("Action", null).show();
                        return;
                    }

                    TextView txtRes = findViewById(R.id.res_unique);
                    Random r = new Random();
                    txtRes.setText(Integer.toString(r.nextInt(numEnd - numStart) + numStart));
                    copy.setVisibility(View.VISIBLE);
                }
            }
        });

        Button btnSelect = findViewById(R.id.select);
        btnSelect.setEnabled(false);
        btnSelect.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                int teams_count = teamsActivity.getCount();
                Random r = new Random();
                int pos = r.nextInt(teams_count);
                TextView selected_team = findViewById(R.id.selected_team);
                String selected = (String) teamsActivity.getItem(pos);

                selected_team.setText(selected);
            }
        });

        Button copy = findViewById(R.id.copy);
        copy.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                TextView unique = findViewById(R.id.res_unique);
                String strUnique = unique.getText().toString();

                ClipboardManager clipboard = (ClipboardManager) getSystemService(Context.CLIPBOARD_SERVICE);
                ClipData clip = ClipData.newPlainText("unique", strUnique);
                clipboard.setPrimaryClip(clip);
            }
        });

        Button uniqueSelector = findViewById(R.id.unique_screen);
        uniqueSelector.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                setViews(R.id.action_unique);
            }
        });

        Button teamSelector = findViewById(R.id.teams_screen);
        teamSelector.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                setViews(R.id.action_teams);
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        MenuItem verItem = menu.findItem(R.id.version);
        verItem.setEnabled(false);

        try {
            PackageInfo pInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
            String version = pInfo.versionName;
            verItem.setTitle(getString(R.string.app_version) + version);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        setViews(id);

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case FILE_SELECT_CODE:
                if (resultCode == RESULT_OK) {
                    // Get the Uri of the selected file
                    Uri uri = data.getData();
                    // Get the path
                    String path = uri.getPath();

                    // Get the file instance
                    try {
                        File file = new File(new URI(path));
                    } catch (URISyntaxException e) {
                        e.printStackTrace();
                    }
                    // Initiate the upload
                }
                break;
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    private void setList(ArrayList<String> list) {
        final ListView listview = findViewById(R.id.teams_list);
        final ArrayAdapter adapter = new ArrayAdapter(this,
                android.R.layout.simple_list_item_1, list);

        TextView selectedTeam = findViewById(R.id.selected_team);
        Button btnSelect = findViewById(R.id.select);
        if (list.size() > 0) {
            btnSelect.setEnabled(true);
        }

        teamsActivity = new TeamsActivity(list, context, btnSelect, selectedTeam);
        listview.setAdapter(teamsActivity);
    }

    private void setViews(int id) {
        LinearLayout unique_view = findViewById(R.id.unique_layout);
        LinearLayout teams_view = findViewById(R.id.teams_layout);
        LinearLayout selector = findViewById(R.id.screen_selector);
        FloatingActionButton fab = findViewById(R.id.fab);

        switch (id) {
            case R.id.action_unique:
                selector.setVisibility(View.GONE);
                unique_view.setVisibility(View.VISIBLE);
                teams_view.setVisibility(View.GONE);
                fab.hide();
                break;
            case R.id.action_teams:
                selector.setVisibility(View.GONE);
                unique_view.setVisibility(View.GONE);
                teams_view.setVisibility(View.VISIBLE);
                fab.show();
                HashMap<String, String> team_read_map = new HashMap<>();

                try {
                    File file = new File(context.getFilesDir(), context.getPackageName());
                    File team_file = new File(file, getString(R.string.teams));
                    FileInputStream f = new FileInputStream(team_file);
                    ObjectInputStream s = new ObjectInputStream(f);
                    team_read_map = (HashMap<String, String>) s.readObject();
                    s.close();
                    f.close();
                } catch (IOException | ClassNotFoundException e) {
                    break;
                }

                String[] values = team_read_map.values().toArray(new String[0]);
                list.clear();

                for (int i = 0; i < values.length; ++i) {
                    list.add(values[i]);
                }

                setList(list);
                break;
        }
    }

    public void importData(View view)
    {
        showFileChooser();
    }

    private void showFileChooser() {
        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
        intent.setType("*/*");
        intent.addCategory(Intent.CATEGORY_OPENABLE);

        try {
            startActivityForResult(
                    Intent.createChooser(intent, "Select a File to Upload"),
                    FILE_SELECT_CODE);
        } catch (android.content.ActivityNotFoundException ex) {
            // Potentially direct the user to the Market with a Dialog
            Toast.makeText(this, "Please install a File Manager.",
                    Toast.LENGTH_SHORT).show();
        }
    }
}
